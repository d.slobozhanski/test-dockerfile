﻿FROM  mcr.microsoft.com/dotnet/sdk:5.0.102-1-alpine3.12-amd64 as build

COPY foo-bar ./foo-bar
WORKDIR foo-bar
RUN dotnet build -c Release

FROM mcr.microsoft.com/dotnet/runtime:5.0.2-alpine3.12-amd64 as runtime
COPY --from=build /foo-bar/bin/ ./foo-bar/bin
ENTRYPOINT ["dotnet", "/foo-bar/bin/Release/net5.0/foo-bar.dll"]