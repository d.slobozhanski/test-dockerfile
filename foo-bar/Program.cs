﻿using System;

namespace foo_bar
{
    class Program
    {
        static int Main(string[] args)
        {
            switch (args.Length)
            {
                case 0:
                    Console.Out.Write("Please specify a word to check for polyndrome as an argument");
                    return 1;
                case > 1:
                    Console.Out.Write("Please specify only one argument");
                    return 1;
            }
            
            var word = args[0];
            Console.Out.Write(FindPolyndrome(word).ToString());
            return 0;
        }
        
        private static bool FindPolyndrome(string word)
        {
            var chars = word.ToCharArray();
            for (var leftIndex = 0; chars.Length / 2 >= leftIndex; leftIndex++)
            {
                var rightIndex = chars.Length - leftIndex - 1;
                
                if (chars[leftIndex] != chars[rightIndex])
                {
                    return false;
                }
            }

            return true;
        }
    }
}
